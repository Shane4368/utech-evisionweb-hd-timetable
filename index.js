getCurrentTab().then(tab =>
{
    if (!tab.url.startsWith("https://evisionweb.utech.edu.jm/sipr/sits.urd/run/"))
    {
        document.getElementById("warning").style.display = "block";
    }
    else
    {
        const captureButton = document.getElementById("capture");
        captureButton.style.display = "block";

        captureButton.addEventListener("click", () =>
        {
            chrome.scripting.executeScript({
                target: { tabId: tab.id },
                function: cloneTimetable
            });
        });
    }
});

function cloneTimetable()
{
    const newWindow = window.open("");

    if (newWindow !== null)
    {
        const timetable = document.getElementById("timetable");
        newWindow.document.write("<style>");
        newWindow.document.write("body{ max-width: calc(100% - 2em); }");
        newWindow.document.write("table{ font-family: system-ui; border-collapse: collapse; }");
        newWindow.document.write("tr th, tr td{ vertical-align: top; }");
        newWindow.document.write("th, td{ border: 1px solid black; }");
        newWindow.document.write("caption{ font-weight: bold; }");
        newWindow.document.write("</style>");
        newWindow.document.write(timetable.innerHTML);
        newWindow.document.close();

        const text = "Once you close this dialog your timetable should be visible. " +
            "To save your timetable as an image, do the following:\n" +
            "1. Open Developer Tools\n" +
            "2. Right-click <html> from Elements tab in Developer Tools\n" +
            "3. Click \"Capture node screenshot\"";

        newWindow.alert(text);
        // newWindow.print(); // uncomment to save as PDF
    }
}

function getCurrentTab()
{
    return chrome.tabs.query({ active: true, currentWindow: true }).then(x => x[0]);
}